package com.maxst.www.sqltest;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Charles on 2016. 5. 24..
 */
public class Person implements Parcelable {
    private long _id;
    private String name;
    private int age;
    private long createdAt;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this._id);
        dest.writeString(this.name);
        dest.writeInt(this.age);
        dest.writeLong(this.createdAt);
    }

    public Person() {
    }

    protected Person(Parcel in) {
        this._id = in.readLong();
        this.name = in.readString();
        this.age = in.readInt();
        this.createdAt = in.readLong();
    }

    public static final Parcelable.Creator<Person> CREATOR = new Parcelable.Creator<Person>() {
        @Override
        public Person createFromParcel(Parcel source) {
            return new Person(source);
        }

        @Override
        public Person[] newArray(int size) {
            return new Person[size];
        }
    };
}
