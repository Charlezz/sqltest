package com.maxst.www.sqltest;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Charles on 2016. 5. 24..
 */
public class MySQLiteOpenHelper extends SQLiteOpenHelper implements DBConstant.Person {

    private static String DB_NAME = "SQLiteTest";
    private static int DB_VERSION = 2;

    public MySQLiteOpenHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable = "CREATE TABLE " + TABLE_NAME + " ("
                + FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + FIELD_NAME + " TEXT, "
                + FIELD_AGE + " TEXT, "
                + FIELD_CREATED_AT + "TIMESTAMP DEFAULT CURRENT_TIMESTAMP);";
        db.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}

