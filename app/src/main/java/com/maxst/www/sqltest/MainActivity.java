package com.maxst.www.sqltest;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    private Context context;

    private ListView listView;
    private ArrayAdapter<String> mAdapter;

    private CharSequence[] items = new CharSequence[]{"삽입", "수정", "전체삭제", "조회"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("DB 테스트")
                        .setItems(items, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which) {
                                    case 0:
                                        add();
                                        break;
                                    case 1:
                                        modify();
                                        break;
                                    case 2:
                                        SQLManager.getInstance().deleteAll();
                                        break;
                                    case 3:
                                        mAdapter.clear();

                                        for (Person person : SQLManager.getInstance().getList()) {
                                            String item = person.getName() + ":" + person.getAge();
                                            mAdapter.add(item);
                                        }

                                        mAdapter.notifyDataSetChanged();
                                        break;
                                }
                                dialog.dismiss();
                            }
                        })
                        .create()
                        .show();

            }
        });


        listView = (ListView) findViewById(R.id.listView);
        mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);

        listView.setAdapter(mAdapter);

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                //delete


                return false;
            }

        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void add() {

        View view = LayoutInflater.from(context).inflate(R.layout.dialog_add_person, null);
        final EditText editName = (EditText) view.findViewById(R.id.name);
        final EditText editAge = (EditText) view.findViewById(R.id.age);
        new AlertDialog.Builder(context)
                .setView(view)
                .setPositiveButton("저장", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Person p = new Person();
                        p.setName(editName.getText().toString());
                        p.setAge(Integer.valueOf(editAge.getText().toString()));
                        SQLManager.getInstance().add(p);
                    }
                })
                .create()
                .show();

    }

    public void modify() {

        View view = LayoutInflater.from(context).inflate(R.layout.dialog_add_person, null);
        final EditText editName = (EditText) view.findViewById(R.id.name);
        final EditText editAge = (EditText) view.findViewById(R.id.age);
        new AlertDialog.Builder(context)
                .setView(view)
                .setPositiveButton("저장", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Person p = new Person();
                        p.setName(editName.getText().toString());
                        p.setAge(Integer.valueOf(editAge.getText().toString()));
                        SQLManager.getInstance().add(p);
                    }
                })
                .create()
                .show();

    }

}
