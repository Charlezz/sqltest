package com.maxst.www.sqltest;

import android.app.Application;
import android.content.Context;

/**
 * Created by Charles on 2016. 5. 24..
 */
public class MyApplication extends Application {


    private static Context context;

    public static Context getContext() {
        return context;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
    }
}
