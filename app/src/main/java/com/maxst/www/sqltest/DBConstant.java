package com.maxst.www.sqltest;

/**
 * Created by Charles on 2016. 5. 24..
 */
public class DBConstant {

    public interface Person {
        String TABLE_NAME = "Person";
        String FIELD_ID = "Id";
        String FIELD_NAME = "Name";
        String FIELD_AGE = "Age";
        String FIELD_CREATED_AT = "Created At";
    }


}
