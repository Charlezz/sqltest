package com.maxst.www.sqltest;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by Charles on 2016. 5. 24..
 */
public class SQLManager {

    public static final String TAG = SQLManager.class.getSimpleName();


    private static SQLManager instance = new SQLManager();

    private MySQLiteOpenHelper dbHelper;
    private SQLiteDatabase db;


    public static SQLManager getInstance() {
        return instance;
    }

    private SQLManager() {
        dbHelper = new MySQLiteOpenHelper(MyApplication.getContext());
    }

    public void add(Person person) {
        db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DBConstant.Person.FIELD_NAME, person.getName());
        values.put(DBConstant.Person.FIELD_AGE, person.getAge());
        db.insert(DBConstant.Person.TABLE_NAME, null, values);
        db.close();
    }

    public void delete(long id) {
        db = dbHelper.getWritableDatabase();
        String whereClause = DBConstant.Person.FIELD_ID + " = ?";
        String[] whereArgs = {String.valueOf(id)};
        db.delete(DBConstant.Person.TABLE_NAME, whereClause, whereArgs);
        db.close();
    }

    public void update(Person person) {
        db = dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DBConstant.Person.FIELD_ID, person.get_id());
        values.put(DBConstant.Person.FIELD_NAME, person.getName());
        values.put(DBConstant.Person.FIELD_AGE, person.getAge());
        values.put(DBConstant.Person.FIELD_CREATED_AT, person.getCreatedAt());

        String whereClause = DBConstant.Person.FIELD_ID + " = ?";
        String[] whereArgs = {String.valueOf(person.get_id())};
        db.update(DBConstant.Person.TABLE_NAME, values, whereClause, whereArgs);
        db.close();
    }

    public ArrayList<Person> getList() {

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String[] columns = new String[]{
                DBConstant.Person.FIELD_ID,
                DBConstant.Person.FIELD_NAME,
                DBConstant.Person.FIELD_AGE,
                DBConstant.Person.FIELD_CREATED_AT
        };

        String selection = null;
        String[] selectionArgs = null;
        String groupBy = null;
        String having = null;
        String orderBy = null;

        Cursor c = db.query(DBConstant.Person.TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy);




        ArrayList<Person> people = new ArrayList<>();
        while (c.moveToNext()) {
            Person person = new Person();
            person.set_id(c.getLong(c.getColumnIndex(DBConstant.Person.FIELD_ID)));
            person.setName(c.getString(c.getColumnIndex(DBConstant.Person.FIELD_NAME)));
            person.setAge(c.getInt(c.getColumnIndex(DBConstant.Person.FIELD_AGE)));
            person.setCreatedAt(c.getLong(c.getColumnIndex(DBConstant.Person.FIELD_ID)));
            people.add(person);
        }
        db.close();
        c.close();
        return people;

    }

    public void deleteAll() {
        for (Person person : getList()) {
            delete(person.get_id());
        }
    }

}
